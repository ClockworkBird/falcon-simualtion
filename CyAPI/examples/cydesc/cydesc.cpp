
/*Summary
The application cydesc is used to open the device with cypress GUID and get the device descriptor
*/



#include <windows.h>

#include <stdio.h>
#include "cyapi.h"
#include <conio.h>
#include <string>
#include <iostream>
#include <cstring>
#include <atlstr.h>

using namespace std;

CCyUSBDevice *USBDevice;
USB_DEVICE_DESCRIPTOR descr;

static const int				MAX_QUEUE_SZ = 64;
static const int				VENDOR_ID	= 0x0547;
static const int				PRODUCT_ID	= 0x1004; 

// These declared static because accessed from the static XferLoop method
// XferLoop is static because it is used as a separate thread.

CCyUSBEndPoint		*EndPt;
int					PPX = 1;
int					QueueSize = 2;
int					TimeOut = 300;
bool				bShowData = true;
bool				bStreaming = true;
bool				bHighSpeedDevice;




	
void Display16Bytes(PUCHAR data)
{	
	unsigned __int16 *data16 = new unsigned __int16[512]; 
	//string xData = "";
	for (int i=0; i < 512; i+=1)
		data16[i] = (__int16)data[i*2+1]*256 + data[i*2];

	for (int i=0; i < 512; i+=8)
	//CString.Format("%s", data);
	//	xData->append(data[i]);
	//	//sprintf(xData, "0x%02x ", data[i]);
		printf("0x%04x 0x%04x 0x%04x 0x%04x 0x%04x 0x%04x 0x%04x 0x%04x \n", data16[i], data16[i+1], data16[i+2], data16[i+3], data16[i+4], data16[i+5], data16[i+6], data16[i+7]);
	delete [] data16;
}

void AbortXferLoop(int pending, PUCHAR *buffers, CCyIsoPktInfo **isoPktInfos, PUCHAR *contexts, OVERLAPPED inOvLap [])
		 {
			EndPt->Abort();
			long len = EndPt->MaxPktSize * PPX;

			for (int j=0; j< QueueSize; j++) 
			{ 
				if (j<pending)
				{
					if (!EndPt->WaitForXfer(&inOvLap[j], TimeOut)) 
					{
						EndPt->Abort();
						if (EndPt->LastError == ERROR_IO_PENDING)
							WaitForSingleObject(inOvLap[j].hEvent,2000);
					}

					EndPt->FinishDataXfer(buffers[j], len, &inOvLap[j], contexts[j]);
				}

				CloseHandle(inOvLap[j].hEvent);

				delete [] buffers[j];
				delete [] isoPktInfos[j];
			}

			delete [] buffers;
			delete [] isoPktInfos;
			delete [] contexts;

					
			bStreaming = false;

			//StartButton->Text = "Start";
			//StartButton->BackColor = Color::Aquamarine;
			//StartButton->Refresh();

			//EptsBox->Enabled	= true;
			//PpxBox->Enabled		= true;
			//QueueBox->Enabled	= true;
			//TimeoutBox->Enabled	= true;
			//ShowBox->Enabled	= true;
		 }

		// This method executes in it's own thread.  The thread gets re-launched each time the
		// Start button is clicked (and the thread isn't already running).
		void XferLoop()
		{
			long BytesXferred = 0;
			unsigned long Successes = 0;
			unsigned long Failures = 0;
			int i = 0;

			// Allocate the arrays needed for queueing
			PUCHAR			*buffers		= new PUCHAR[QueueSize];
			CCyIsoPktInfo	**isoPktInfos	= new CCyIsoPktInfo*[QueueSize];
			PUCHAR			*contexts		= new PUCHAR[QueueSize];
			OVERLAPPED		inOvLap[MAX_QUEUE_SZ];

			long len = EndPt->MaxPktSize * PPX; // Each xfer request will get PPX isoc packets

			EndPt->SetXferSize(len);
			
			// Allocate all the buffers for the queues
			for (i=0; i< QueueSize; i++) 
			{ 
				buffers[i]        = new UCHAR[len];
				isoPktInfos[i]    = new CCyIsoPktInfo[PPX];
				inOvLap[i].hEvent = CreateEvent(NULL, false, false, NULL);

				memset(buffers[i],0,len);
			}
			
			//DateTime t1 = DateTime::Now;	// For calculating xfer rate
			
			// Queue-up the first batch of transfer requests
			for (i=0; i< QueueSize; i++)	
			{
				contexts[i] = EndPt->BeginDataXfer(buffers[i], len, &inOvLap[i]);
				if (EndPt->NtStatus || EndPt->UsbdStatus) // BeginDataXfer failed
				{
					printf("Xfer request rejected. NTSTATUS = %d", EndPt->NtStatus);
					AbortXferLoop(i+1, buffers,isoPktInfos,contexts, inOvLap);
					return;
				}
			}

			i=0;	

			// The infinite xfer loop.
			for (;bStreaming;)		
			{
				long rLen = len;	// Reset this each time through because
									// FinishDataXfer may modify it

				if (!EndPt->WaitForXfer(&inOvLap[i], TimeOut))
				{
					EndPt->Abort();
					if (EndPt->LastError == ERROR_IO_PENDING)
						WaitForSingleObject(inOvLap[i].hEvent,2000);
				}

				if (EndPt->Attributes == 1) // ISOC Endpoint
				{	
					if (EndPt->FinishDataXfer(buffers[i], rLen, &inOvLap[i], contexts[i], isoPktInfos[i])) 
					{			
	  					CCyIsoPktInfo *pkts = isoPktInfos[i];
						for (int j=0; j< PPX; j++) 
						{
							if (pkts[j].Status == 0) 
							{
								BytesXferred += pkts[j].Length;

								if (bShowData)
									Display16Bytes(buffers[i]);

								Successes++;
							}
							else
								Failures++;

							pkts[j].Length = 0;	// Reset to zero for re-use.
						}

					} 
					else
						Failures++; 
			
				} 

				else // BULK Endpoint
				{
					if (EndPt->FinishDataXfer(buffers[i], rLen, &inOvLap[i], contexts[i])) 
					{			
						Successes++;
						BytesXferred += len;

						if (bShowData)
							Display16Bytes(buffers[i]);
					} 
					else
						Failures++; 
				}


				//if (BytesXferred < 0) // Rollover - reset counters
				//{
				//	BytesXferred = 0;
				//	t1 = DateTime::Now;
				//}

				// Re-submit this queue element to keep the queue full
				contexts[i] = EndPt->BeginDataXfer(buffers[i], len, &inOvLap[i]);
				if (EndPt->NtStatus || EndPt->UsbdStatus) // BeginDataXfer failed
				{
					printf("Xfer request rejected. NTSTATUS = %d",EndPt->NtStatus);
					AbortXferLoop(QueueSize,buffers,isoPktInfos,contexts,inOvLap);
					return;
				}

				i++;
				if (i == 2)
					bStreaming = false;

				//if (i == QueueSize) //Only update the display once each time through the Queue
				//{
				//	i=0;
				//	ShowStats(t1, BytesXferred, Successes, Failures);					
				//}

			}  // End of the infinite loop

			// Memory clean-up
			AbortXferLoop(QueueSize,buffers,isoPktInfos,contexts,inOvLap);
		}

void main()
{

	bStreaming = true;

    USBDevice = new CCyUSBDevice(NULL);   // Create an instance of CCyUSBDevice

	EndPt = USBDevice->BulkInEndPt;
    printf("device count = %d \n",USBDevice->DeviceCount());


	USBDevice->Open(0);
	PUCHAR inBuf = new UCHAR[1024];
	unsigned __int16* output_buf = new unsigned __int16[1024];
	LONG length = 1024;
	OVERLAPPED outOvLap, inOvLap;

	for (int i=0;i<2;i++) {
		length = 1024;

		inOvLap.hEvent = CreateEvent(NULL, false, false, "CYUSB_IN");
		ZeroMemory(inBuf, 1024);

		// Just to be cute, request the return data before initiating the loopback
		UCHAR *inContext = EndPt->BeginDataXfer(inBuf, length, &inOvLap);
		EndPt->WaitForXfer(&inOvLap,100);
		EndPt->FinishDataXfer(inBuf, length, &inOvLap,inContext);
		CloseHandle(inOvLap.hEvent);

		printf("Length of received data: %d\n", length);

		for (int i = 0; i < length; i++) {
			output_buf[i] = inBuf[i];
	
		}

		for (int i = 0; i < length; i += 8) {
			printf("0x%02x 0x%02x 0x%02x 0x%02x 0x%02x 0x%02x 0x%02x 0x%02x \n", output_buf[i], output_buf[i+1], output_buf[i+2],output_buf[i+3],output_buf[i+4], output_buf[i+5], output_buf[i+6],output_buf[i+7]);		
		}

		delete [] inContext;

	}

			delete [] inBuf;
		delete [] output_buf;

    //USBDevice->GetDeviceDescriptor(&descr);
	//XferLoop();

    //USBDevice->Close();
	USBDevice->~CCyUSBDevice();
	delete USBDevice;
	_getch();

}

