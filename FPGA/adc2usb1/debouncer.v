`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// $Source: /root/cvs/cvsdir/dvbt2/fpga/ldpc/debouncer/debouncer.v,v $
// $Revision: 1.2 $
// $Date: 2008-09-17 08:57:43 $
// $Author: kocks $
// Debouncer
//////////////////////////////////////////////////////////////////////////////////

module debouncer(
    input			clk,
	input			button,
	output			single_shot_out
    );

	parameter COUNTER_LEN		= 27;
	parameter COUNTER_EVAL_LEN	= 23;
	
	reg		[COUNTER_LEN-1 : 0]	debounce_counter = 0;
	
	assign single_shot_out = (debounce_counter == {COUNTER_EVAL_LEN{1'b1}});
	
	always @ (posedge clk)
		begin
			if (button)
				begin
					debounce_counter	<= debounce_counter + 1;
				end
			else
				begin
					debounce_counter	<= 0;
				end
		end
endmodule

//	%Log%