`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    15:50:52 06/04/2013 
// Design Name: 
// Module Name:    counter_32 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: nbit counter, 32bit is default
//
//////////////////////////////////////////////////////////////////////////////////
module counter_n #(
		parameter	pCounterWidth = 32,
		parameter	pFrencencyDiv = 16) (
		input			iClk,
		input			iReset,
		input			iEnable,
		output		[pCounterWidth - 1 : 0]	oCounter,
		output 		oDiv
	);
	
	reg		[pCounterWidth - 1 : 0]	rCounter;
	
	always @ (posedge iClk)
		begin
			if (iReset)
				begin
					rCounter	<= 0;
				end
			else
				begin
					if (iEnable)
						begin
							rCounter	<= rCounter + 1;
						end
					else
						begin
							rCounter	<= rCounter;
						end
				end
		end

	assign oCounter 	= rCounter;
	assign oDiv 		= rCounter[pFrencencyDiv];

endmodule
