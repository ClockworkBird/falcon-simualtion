`timescale 1ns / 1ps

// USB handling

//`define CHANGE_ENDIANESS

module usb_handling(
	output	[15	: 0]	oUsbData, 			// {USB_FD_15to8, USB_Flash_DQ}
	
	output				oUsbReadN,			// slave-FIFO read (USB_SLRD_L)
	output				oUsbWriteN,			// slave-FIFO write (USB_SLWR_L)
	
	output				oUsbOutputEnableN,	// slave-FIFO output enable (USB_SLOE_L)
	output		[1:0]	oUsbFifoAddr,		// slave-FIFO address select (USB_FIFOADDR[1:0])
	output				oUsbChipSelectN,	// slave-FIFO enable (USB_SLCS_L)
	
	input				iWriteEvent,
	input		[15:0]	iData
    );
	
	/*
	 * PARAMETERS
	 */
	parameter DEFAULT_FIFO_ADDRESS			= 2'b00; // EP2
	
	/*
	 * ASSIGNS
	 */
	`ifdef CHANGE_ENDIANESS
	assign oUsbData ={change_endian(iData[15:8]), change_endian(iData[7:0])};
	`else
	assign oUsbData ={iData[15:8], iData[7:0]};
	`endif

	assign oUsbWriteN			= !iWriteEvent;
	assign oUsbFifoAddr			= DEFAULT_FIFO_ADDRESS;

	assign oUsbChipSelectN		= 0; // can be tied to zero since the FIFO bus is only used by one slave device
	assign oUsbOutputEnableN	= 1; // for FPGA-to-PC transmission
	assign oUsbReadN			= 1; // for FPGA-to-PC transmission

	/*
	 * FUNCTION
	 */
	function [7 : 0] change_endian(input [7 : 0] dataIn);
		integer k;
		begin
			for (k=0; k<8; k=k+1)
				change_endian[k] = dataIn[7-k];
		end
	endfunction

endmodule
